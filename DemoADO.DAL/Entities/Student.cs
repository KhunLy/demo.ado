﻿using System;


namespace DemoADO.DAL.Entities
{
    public class Student
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FisrtName { get; set; }
        public string Matricule { get; set; }
        public DateTime? BirthDate { get; set; }
        public bool AnanasFan { get; set; }
        public DateTime? InscriptionDate { get; set; }
        public int YearResult { get; set; }
        public int? SectionId { get; set; }
    }
}
