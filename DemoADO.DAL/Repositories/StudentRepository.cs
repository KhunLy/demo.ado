﻿using DemoADO.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;

namespace DemoADO.DAL.Repositories
{
    public class StudentRepository : BaseRepository<Student>
    {
        public StudentRepository(string connectionString, string providerName) : base(connectionString, providerName)
        {
        }

        public IEnumerable<Student> GetBySectionId(int sectionId)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "SELECT * FROM Student WHERE SectionId = @p1";
                Dictionary<string, object> dico = new Dictionary<string, object>
                {
                    { "@p1", sectionId }
                };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                IDataReader r = cmd.ExecuteReader();
                while (r.Read())
                {
                    yield return ReaderToEntityMapper(r);
                }
            }
        }
        
        #region Ancienne Version
        //private string _connectionString;
        //private string _providerName;
        //public StudentRepository(string connectionString, string providerName)
        //{
        //    _connectionString = connectionString;
        //    _providerName = providerName;
        //}
        //public IEnumerable<Student> Get(int limit = 20, int offset = 0)
        //{
        //    using (IDbConnection conn = GetConnection())
        //    {
        //        conn.Open();
        //        string query = "SELECT * FROM Student ORDER BY Id DESC OFFSET @p2 ROWS FETCH NEXT @p1 ROWS ONLY";
        //        Dictionary<string, object> dico = new Dictionary<string, object>
        //        {
        //            { "@p1", limit },
        //            { "@p2", offset }
        //        };
        //        IDbCommand cmd = CreateCommand(conn, query, dico);
        //        IDataReader r = cmd.ExecuteReader();
        //        while (r.Read())
        //        {
        //            yield return ReaderToEntityMapper(r);
        //        }
        //    }
        //}
        //public Student GetById(int id)
        //{
        //    using (IDbConnection conn = GetConnection())
        //    {
        //        conn.Open();
        //        string query = "SELECT * FROM Student WHERE Id = @p1";
        //        Dictionary<string, object> dico = new Dictionary<string, object>
        //        {
        //            { "@p1", id }
        //        };
        //        IDbCommand cmd = CreateCommand(conn, query, dico);
        //        IDataReader r = cmd.ExecuteReader();
        //        if (r.Read())
        //        {
        //            return ReaderToEntityMapper(r);
        //        }
        //        return null;
        //    }
        //}
        //public int Insert(Student s)
        //{
        //    using (IDbConnection conn = GetConnection())
        //    {
        //        conn.Open();
        //        string query
        //            = "INSERT INTO Student OUTPUT INSERTED.Id VALUES(";
        //        IEnumerable<PropertyInfo> props
        //            = typeof(Student).GetProperties();
        //        foreach (PropertyInfo p in props)
        //        {
        //            if (p.Name != "Id")
        //                query += "@" + p.Name + ",";
        //        }
        //        query = query.Substring(0, query.Length - 1);
        //        query += ")";
        //        Dictionary<string, object> parameters
        //            = new Dictionary<string, object>();
        //        foreach (PropertyInfo p in props)
        //        {
        //            if (p.Name != "Id")
        //                parameters.Add("@" + p.Name, p.GetValue(s) ?? DBNull.Value);
        //        }
        //        IDbCommand cmd = CreateCommand(conn, query, parameters);
        //        int id = (int)cmd.ExecuteScalar();
        //        return id;
        //    }
        //}
        //public bool Update(Student s)
        //{
        //    using (IDbConnection conn = GetConnection())
        //    {
        //        conn.Open();
        //        string query = "UPDATE Student SET ";
        //        IEnumerable<PropertyInfo> props
        //            = typeof(Student).GetProperties();
        //        foreach (PropertyInfo p in props)
        //        {
        //            if (p.Name != "Id")
        //                query += $"{p.Name} = @{p.Name},";
        //        }
        //        query = query.Substring(0, query.Length - 1);
        //        query += " WHERE Id = @Id";
        //        Dictionary<string, object> parameters
        //            = new Dictionary<string, object>();
        //        foreach (PropertyInfo p in props)
        //        {
        //            parameters.Add("@" + p.Name, p.GetValue(s) ?? DBNull.Value);
        //        }

        //        IDbCommand cmd = CreateCommand(conn, query, parameters);
        //        int nb = cmd.ExecuteNonQuery();
        //        return nb != 0;
        //    }
        //}
        //public bool Delete(int id)
        //{
        //    using (IDbConnection conn = GetConnection())
        //    {
        //        conn.Open();
        //        string query = "DELETE FROM Student WHERE Id = @p1";
        //        Dictionary<string, object> parameters =
        //            new Dictionary<string, object>()
        //            {
        //                { "@p1", id }
        //            };
        //        IDbCommand cmd = CreateCommand(conn, query, parameters);
        //        int nbLines = cmd.ExecuteNonQuery();
        //        return nbLines != 0;
        //    }
        //}

        //private IDbConnection GetConnection()
        //{
        //    DbProviderFactory factory = DbProviderFactories.GetFactory(_providerName);

        //    // crée ma connection
        //    IDbConnection connection = factory.CreateConnection();

        //    // set ma connection à ma connection
        //    connection.ConnectionString = _connectionString;

        //    return connection;
        //}
        //private IDbCommand CreateCommand(IDbConnection conn, string text, Dictionary<string, object> parameters = null)
        //{
        //    IDbCommand cmd = conn.CreateCommand();
        //    cmd.CommandText = text;
        //    if (parameters != null)
        //    {
        //        foreach (KeyValuePair<string, object> kvp in parameters)
        //        {
        //            IDataParameter p = cmd.CreateParameter();
        //            p.ParameterName = kvp.Key;
        //            p.Value = kvp.Value;
        //            cmd.Parameters.Add(p);
        //        }
        //    }
        //    return cmd;
        //}
        //private Student ReaderToEntityMapper(IDataReader r)
        //{
        //    Student s = new Student();
        //    IEnumerable<PropertyInfo> properties = s.GetType().GetProperties();
        //    foreach (PropertyInfo p in properties)
        //    {
        //        p.SetValue(s, r[p.Name] == DBNull.Value ? null : r[p.Name]);
        //    }
        //    return s;
        //    //return new Student
        //    //{
        //    //    Id = (int)r["id"],
        //    //    LastName = (string)r["LastName"],
        //    //    FirstName = (string)r["FirstName"],
        //    //    Matricule = (string)r["Matricule"],
        //    //    BirthDate = r["BirthDate"] as DateTime?,
        //    //    SectionId = r["SectionId"] as int?,
        //    //    AnanasFan = (bool)r["AnanasFan"],
        //    //    InscriptionDate = r["InscriptionDate"] as DateTime?,
        //    //    YearResult = (int)r["YearResult"]
        //    //};
        //} 
        #endregion
    }
}
