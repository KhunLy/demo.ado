﻿using DemoADO.DAL.Entities;
using DemoADO.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoADO2
{
    class Program
    {
        static string ServerName = @"DESKTOP-HKVTM9G\SQLSERVER";
        static string DbName = "DemoADO";
        static string Username = "sa";
        static string Password = "test1234=";


        static void Main(string[] args)
        {
            string connectionString
                = $@"Data Source={ServerName};Initial Catalog={DbName};User ID={Username};Pwd={Password}";
            //= $@"Server Name={ServerName};Initial Catalog={DbName};Integrated Security=SSPI;";

            //// crée les outils pour construire un connection SQL
            //DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.SqlClient");

            //// crée ma connection
            //IDbConnection connection = factory.CreateConnection();

            //// set ma connection à ma connection
            //connection.ConnectionString = connectionString;

            //// démarrer la connection vers le server SQL
            //connection.Open();

            ////IDbCommand cmd = factory.CreateCommand();
            ////cmd.Connection = connection;

            //IDbCommand cmd = connection.CreateCommand();

            //cmd.CommandText = "SELECT * FROM Student";
            //IDataReader reader = cmd.ExecuteReader();

            //while(reader.Read())
            //{
            //    // traitement sur chacune des lignes récupérées
            //    Console.WriteLine(reader["LastName"]);
            //}

            //// fermer la connection au serveur SQL
            //connection.Close();

            //Console.WriteLine("Choisissez une lettre");

            //string letter = Console.ReadLine();

            //connection.Open();

            //IDbCommand cmd2 = connection.CreateCommand();
            //cmd2.CommandText = "SELECT * FROM Section WHERE Name LIKE @param";
            //IDataParameter p = cmd2.CreateParameter();
            //p.ParameterName = "@param";
            //p.Value = letter + "%";
            //cmd2.Parameters.Add(p);

            //IDataReader r = cmd2.ExecuteReader();
            //while(r.Read())
            //{
            //    Console.WriteLine(r["name"]);
            //}

            //connection.Close();

            SectionRepository repo = new SectionRepository(
                connectionString,
                "System.Data.SqlClient"
            );

            //Section section = repo.GetById(3);

            //Console.WriteLine(section.Name);

            //IEnumerable<Section> sections = repo.Get();
            //foreach(Section s in sections)
            //{
            //    Console.WriteLine(s.Id + "-"+s.Name);
            //}

            //Console.WriteLine("Quel section voulez delete?");
            //int reponse = int.Parse(Console.ReadLine());
            //if(repo.Delete(reponse))
            //{
            //    Console.WriteLine("La section numero " + reponse + "a été supprimée");
            //}
            //else{
            //    Console.WriteLine("Rien n'a été supprimé");
            //}

            //string sectionName = Console.ReadLine();
            //Section s = new Section { Name = sectionName };
            //int id = repo.Insert(s);
            //Console.WriteLine("La section numero " + id + " a été insérée");

            StudentRepository SRepo = new StudentRepository(
                connectionString,
                "System.Data.SqlClient"
                );

            IEnumerable<Student> st = SRepo.Get();
            foreach (Student s in st)
            {
                Console.WriteLine(s.LastName + s.FisrtName);
            }
            Student s1 = new Student()
            {
                FisrtName = "Adrien",
                LastName = "Frenay",
                Matricule = "MAT003",
                AnanasFan = false,
                YearResult = 14,
                SectionId = 6,
                Id = 3
            };
            SRepo.Insert(s1);
            //SRepo.Update(s1);


            //SRepo.Delete(3);


            IEnumerable<Student> stSection = SRepo.GetBySectionId(4);
            foreach (Student s in stSection)
            {
                Console.WriteLine(s.FisrtName);
            }
            Console.ReadKey();
        }
    }
}
